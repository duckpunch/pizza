#!/bin/bash

set -e

cd /app && pip install -e .
chown pizza.www-data /app
cd /app/pizza/persistence && alembic upgrade head

uwsgi --uid pizza --gid www-data --master --workers 3 /etc/uwsgi.yaml \
& /usr/sbin/nginx -g 'daemon off; master_process on;'
