import development from './development';
import production from './production';


export function GET() {
    if (process.env.NODE_ENV === 'production') {
        production.GET(...arguments);
    } else {
        development.GET(...arguments);
    }
}


export function POST() {
    if (process.env.NODE_ENV === 'production') {
        production.POST(...arguments);
    } else {
        development.POST(...arguments);
    }
}


export function DELETE() {
    if (process.env.NODE_ENV === 'production') {
        production.DELETE(...arguments);
    } else {
        development.DELETE(...arguments);
    }
}
