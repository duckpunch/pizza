import range from 'lodash/range';


const defaults = {
    '/api/games': {
        game_id: '12345',
        black_id: '67890',
        white_id: '54321',
    },
    '/api/games/12345': {
        id: '12345',
        size: 19,
        komi: 7.5,
        handicap: 5,
    },
    '/api/games/12345/moves': [
        {x: 4, y: 5},
        {x: 5, y: 5},
        {x: 5, y: 4},
        {x: 4, y: 4},
        {x: 3, y: 4},
        {x: 3, y: 5},
        {x: 4, y: 3},
        {x: 4, y: 6},
        {x: 4, y: 7},
        {x: 4, y: 4},
    ],
    '/api/players/67890': {
        id: '67890',
        game_id: '12345',
        color: 'black',
    },
    '/api/players/54321': {
        id: '54321',
        game_id: '12345',
        color: 'white',
    },
};


function GET(url, callback) {
    console.log('GET', url);
    const defaultValue = defaults[url] || null;

    setTimeout(() => {
        if (defaultValue === null) {
            console.log('GET: No value set for', url);
        }
        callback(defaultValue);
    }, 500);
}


function POST(url, body, callback = () => {}, progress = () => {}) {
    console.log('POST', url, body);
    const defaultValue = defaults[url] || null;

    range(10).map(n => {
        return setTimeout(
            () => {progress({
                percent: n * 10,
            });}
        , 5 * n);
    });

    setTimeout(() => {
        if (defaultValue === null) {
            console.log('POST: No value set for', url);
        }
        callback(defaultValue);
    }, 50);
}


function DELETE(url, callback = () => {}) {
    console.log('DELETE', url);
    setTimeout(() => {
        callback();
    }, 500);
}


export default {GET, POST, DELETE};
