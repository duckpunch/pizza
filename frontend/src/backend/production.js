function GET(url, callback) {
    return fetch(url, {credentials: 'same-origin'}).then(
        (response) => {
            response.json().then(callback).catch(callback);
        }
    ).catch(callback);
}


function POST(url, body, callback = () => {}) {
    return fetch(
        url, {
            method: 'POST',
            credentials: 'same-origin',
            body: JSON.stringify(body),
            headers: {
                'content-type': 'application/json',
            },
        },
    ).then(response => {
        response.json().then(callback).catch(callback);
    }).catch(callback);
}


function DELETE(url, callback = () => {}) {
    return fetch(
        url, {
            method: 'DELETE',
            credentials: 'same-origin',
        },
    ).then(callback).catch(callback);
}


export default {GET, POST, DELETE};
