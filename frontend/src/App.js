import React from 'react';
import Button from 'muicss/lib/react/button';
import Input from 'muicss/lib/react/input';
import {BrowserRouter, Route} from 'react-router-dom';
import capitalize from  'lodash/capitalize';
import last from  'lodash/last';
import inRange from  'lodash/inRange';
import isInteger from  'lodash/isInteger';
import initial from  'lodash/initial';
import partialRight from 'lodash/partialRight';
import {CopyToClipboard} from 'react-copy-to-clipboard';

import godash from 'godash';
import {Goban} from 'react-go-board';
import {GET, POST} from './backend';

import './App.css';

function MainLayout(props) {
    return <div className='game'>
        <div className='board'>
            {props.board && <Goban
                board={props.board}
                onCoordinateClick={props.handleCoordinateClick}
                annotations={props.annotations} />}
        </div>
        <div className='meta'>
            {props.children}
            <div className='spacer'>&nbsp;</div>
        </div>
    </div>;
}

function makeHandicappedBoard(size, handicap) {
    const handicapAllowed = size === 19 || size === 13 || size === 9;

    if (handicapAllowed) {
        return godash.handicapBoard(size, handicap);
    } else {
        return new godash.Board(size);
    }
}

class Game extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            board: null,
            annotations: [],
        };
    }

    componentDidMount() {
        const gameId = this.props.match.params.gameHash;
        GET(`/api/games/${gameId}`, this.gameLoaded.bind(this));
    }

    refreshMoves() {
        GET(
            `/api/games/${this.state.game_id}/moves`,
            this.movesLoaded.bind(this),
        );
    }

    gameLoaded(game) {
        this.setState({
            board: makeHandicappedBoard(game.size, game.handicap),
            size: game.size,
            komi: game.komi,
            handicap: game.handicap,
            game_id: game.id,
        });
        GET(`/api/games/${game.id}/moves`, this.movesLoaded.bind(this));
    }

    movesLoaded(moves) {
        const startColor = this.state.handicap > 0 ? godash.WHITE : godash.BLACK;
        const nextColor = moves.length % 2 === 0 ?
            startColor : godash.oppositeColor(startColor);

        let board;
        let illegalMove = null;
        if (moves.length === 0) {
            board = this.state.board;
        } else {
            const firstPart = initial(moves);
            const mostRecent = last(moves);

            board = godash.constructBoard(
                firstPart, makeHandicappedBoard(this.state.size, this.state.handicap), startColor
            );

            const mostRecentMove = new godash.Coordinate(mostRecent.x, mostRecent.y);
            illegalMove = godash.followupKo(board, mostRecentMove, godash.oppositeColor(nextColor));
            board = godash.addMove(board, mostRecentMove, godash.oppositeColor(nextColor));
        }

        this.setState({
            board,
            annotations: moves.length > 0 ? [last(moves)] : [],
            nextColor,
            illegalMove,
        });
        setTimeout(this.refreshMoves.bind(this), 10000);
    }

    render() {
        return <MainLayout board={this.state.board} annotations={this.state.annotations}>
            <div>Komi: {this.state.komi}</div>
            <div>Handicap: {this.state.handicap}</div>
            <div>{capitalize(this.state.nextColor)} is next</div>
        </MainLayout>;
    }
}

class Player extends Game {
    constructor(props) {
        super(props);

        this.state = {
            board: null,
            annotations: [],
            color: '',
        };
    }

    componentDidMount() {
        const playerHash = this.props.match.params.playerHash;

        GET(`/api/players/${playerHash}`, this.playerLoaded.bind(this));
    }

    playerLoaded(player) {
        this.setState({
            color: player.color,
            player_id: player.id,
        });
        GET(`/api/games/${player.game_id}`, this.gameLoaded.bind(this));
    }

    handleCoordinateClick(coordinate) {
        const isNextPlayer = this.state.nextColor === this.state.color;
        const isLegalMove = !(this.state.illegalMove && this.state.illegalMove.equals(coordinate))
        if (isNextPlayer && isLegalMove) {
            this.setState({
                provisionalMove: coordinate,
            });
        }
    }

    submitMove() {
        POST(
            `/api/games/${this.state.game_id}/moves`,
            {
                x: this.state.provisionalMove.x,
                y: this.state.provisionalMove.y,
                player_id: this.state.player_id,
            },
            this.refreshMoves.bind(this),
        );
        this.setState({
            provisionalMove: null,
        });
    }

    render() {
        const playerIsNext = this.state.nextColor === this.state.color;
        const board = this.state.provisionalMove ?
            godash.addMove(
                this.state.board, this.state.provisionalMove, this.state.color
            ) : this.state.board;
        const annotations = this.state.provisionalMove && playerIsNext ?
            [this.state.provisionalMove] : this.state.annotations;

        return <MainLayout board={board} annotations={annotations}
            handleCoordinateClick={this.handleCoordinateClick.bind(this)}>
            {playerIsNext && <Button color='primary'
                disabled={!this.state.provisionalMove}
                onClick={this.submitMove.bind(this)}>
                    Send
                </Button>}
            <div>Komi: {this.state.komi}</div>
            <div>Handicap: {this.state.handicap}</div>
            <div>{capitalize(this.state.nextColor)} is next</div>
        </MainLayout>;
    }
}

class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            size: 19,
            handicap: 0,
            komi: 6.5,
        };
    }

    onInputChange(event, key) {
        const newState = {};
        newState[key] = parseFloat(event.target.value);
        this.setState(newState);
    }

    onCreateGame() {
        const handicapAllowed = (
            this.state.size === 19
            || this.state.size === 13
            || this.state.size === 9
        );
        POST(
            '/api/games',
            {
                size: this.state.size,
                komi: this.state.komi,
                handicap: handicapAllowed ? this.state.handicap : 0,
            },
            createdGame => {
                this.setState({createdGame});
            },
        );
    }

    render() {
        if (this.state.createdGame) {
            const blackLink = `${window.location.origin}/p/${this.state.createdGame.black_id}`;
            const whiteLink = `${window.location.origin}/p/${this.state.createdGame.white_id}`;
            const spectatorLink = `${window.location.origin}/g/${this.state.createdGame.game_id}`;
            return <div className='home'>
                <h1>pizza!</h1>
                <div>
                    <strong>Black: </strong>
                    <a href={blackLink} target='_blank'>{blackLink}</a>
                    <CopyToClipboard text={blackLink}>
                        <a className='copy-url'>Copy</a>
                    </CopyToClipboard>
                </div>
                <div>
                    <strong>White: </strong>
                    <a href={whiteLink} target='_blank'>{whiteLink}</a>
                    <CopyToClipboard text={whiteLink}>
                        <a className='copy-url'>Copy</a>
                    </CopyToClipboard>
                </div>
                <div>
                    <strong>Spectator: </strong>
                    <a href={spectatorLink} target='_blank'>{spectatorLink}</a>
                    <CopyToClipboard text={spectatorLink}>
                        <a className='copy-url'>Copy</a>
                    </CopyToClipboard>
                </div>
            </div>;
        } else {
            const handicapAllowed = (
                this.state.size === 19
                || this.state.size === 13
                || this.state.size === 9
            );
            const validSize = (
                isInteger(parseFloat(this.state.size || 0))
                && inRange(this.state.size, 5, 20)
            );
            const validHandicap = (
                isInteger(parseFloat(this.state.handicap))
                && inRange(this.state.handicap, 0, 10)
            );
            const validSettings = validSize && validHandicap;
            return <div className='home'>
                <h1>pizza!</h1>
                <Input label='Board size' type='number' value={this.state.size || ''}
                    onChange={partialRight(this.onInputChange, 'size').bind(this)}
                    min={5} max={19} />
                <Input label='Handicap' type='number'
                    value={isNaN(this.state.handicap) ? '' : this.state.handicap}
                    onChange={partialRight(this.onInputChange, 'handicap').bind(this)}
                    min={0} max={9} disabled={!handicapAllowed}/>
                <Input label='Komi' type='number' value={this.state.komi || ''}
                    onChange={partialRight(this.onInputChange, 'komi').bind(this)}
                    step={0.5}/>
                <Button color='primary' onClick={this.onCreateGame.bind(this)}
                    disabled={!validSettings}>
                    Create game
                </Button>
            </div>;
        }
    }
}

export default function() {
    return <BrowserRouter><div>
        <Route exact path='/' component={Home} />
        <Route path='/g/:gameHash' component={Game} />
        <Route path='/p/:playerHash' component={Player} />
    </div></BrowserRouter>;
};
