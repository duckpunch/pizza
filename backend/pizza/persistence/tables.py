import os

import sqlalchemy


metadata = sqlalchemy.MetaData()

games = sqlalchemy.Table(
    'games', metadata,
    sqlalchemy.Column('id', sqlalchemy.Unicode, primary_key=True),
    sqlalchemy.Column('size', sqlalchemy.Integer),
    sqlalchemy.Column('komi', sqlalchemy.Numeric(asdecimal=False)),
    sqlalchemy.Column('handicap', sqlalchemy.Integer),
    sqlalchemy.Column(
        'started',
        sqlalchemy.DateTime,
        server_default=sqlalchemy.sql.func.now(),
    ),
)

players = sqlalchemy.Table(
    'players', metadata,
    sqlalchemy.Column('id', sqlalchemy.Unicode, primary_key=True),
    sqlalchemy.Column('color', sqlalchemy.Unicode),
    sqlalchemy.Column('game_id', sqlalchemy.Unicode),
)

moves = sqlalchemy.Table(
    'moves', metadata,
    sqlalchemy.Column('game_id', sqlalchemy.Unicode),
    sqlalchemy.Column('x', sqlalchemy.Integer),
    sqlalchemy.Column('y', sqlalchemy.Integer),
    sqlalchemy.Column('color', sqlalchemy.Unicode),
    sqlalchemy.Column(
        'played',
        sqlalchemy.DateTime,
        server_default=sqlalchemy.sql.func.now(),
    ),
)


def get_connection():
    engine = sqlalchemy.create_engine(os.environ['DATABASE_URL'])
    return engine.begin()
