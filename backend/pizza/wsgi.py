import os
import json
import secrets

import flask
import sqlalchemy
import werkzeug
import voluptuous

from pizza.persistence import tables


def disable_caching(response):
    response.headers['Cache-Control'] = 'no-cache, no-store, must-invalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = 0
    return response


application = flask.Flask(__name__)
application.secret_key = os.environ['FLASK_SECRET_KEY']
application.after_request(disable_caching)


@application.errorhandler(voluptuous.MultipleInvalid)
def invalid_schema(error):
    return 'Invalid schema', 400


@application.route('/api/games', methods=['POST'])
def create_game():
    try:
        raw_game = flask.request.get_json()
    except werkzeug.exceptions.BadRequest:
        return 'Invalid JSON', 400

    if raw_game is None:
        return 'Bad content type', 400

    user_input = voluptuous.Schema({
        voluptuous.Required('size'): int,
        voluptuous.Required('handicap'): int,
        voluptuous.Required('komi'): voluptuous.Number(),
    })(raw_game)

    with tables.get_connection() as connection:
        game_id = secrets.token_urlsafe(8)
        white_id = secrets.token_urlsafe(8)
        black_id = secrets.token_urlsafe(8)

        connection.execute(tables.games.insert().values(
            id=game_id, **user_input,
        ))
        connection.execute(tables.players.insert().values(
            id=white_id, color='white', game_id=game_id,
        ))
        connection.execute(tables.players.insert().values(
            id=black_id, color='black', game_id=game_id,
        ))

    return flask.jsonify(dict(
        game_id=game_id, black_id=black_id, white_id=white_id,
    )), 201


@application.route('/api/games/<game_id>')
def get_game(game_id):
    with tables.get_connection() as connection:
        game = connection.execute(tables.games.select().where(
            tables.games.c.id == game_id
        )).first()

    if game is not None:
        return json.dumps(dict(
            game,
            started=game['started'].isoformat(),
        ))
    else:
        return '', 404


@application.route('/api/players/<player_id>')
def get_player(player_id):
    with tables.get_connection() as connection:
        player = connection.execute(tables.players.select().where(
            tables.players.c.id == player_id
        )).first()

    if player is not None:
        return json.dumps(dict(player))
    else:
        return 'Nothing to see!', 404


@application.route('/api/games/<game_id>/moves')
def get_moves(game_id):
    with tables.get_connection() as connection:
        moves = connection.execute(tables.moves.select().where(
            tables.moves.c.game_id == game_id
        ).order_by(tables.moves.c.played))

    return flask.jsonify(
        list(map(
            lambda m: dict(m, played=m['played'].isoformat()),
            moves,
        ))
    )


@application.route('/api/games/<game_id>/moves', methods=['POST'])
def play_move(game_id):
    try:
        raw_move = flask.request.get_json()
    except werkzeug.exceptions.BadRequest:
        return 'Invalid JSON', 400

    if raw_move is None:
        return 'Bad content type', 400

    user_input = voluptuous.Schema({
        voluptuous.Required('player_id'): str,
        voluptuous.Required('x'): int,
        voluptuous.Required('y'): int,
    })(raw_move)

    with tables.get_connection() as connection:
        player = connection.execute(tables.players.select().where(
            tables.players.c.id == user_input['player_id']
        )).first()

    if player is None:
        return 'Not a real player!', 401

    if player['game_id'] != game_id:
        return 'Can\'t add moves to someone else\'s game!', 401

    with tables.get_connection() as connection:
        game = connection.execute(tables.games.select().where(
            tables.games.c.id == game_id
        )).first()

        last_move = connection.execute(tables.moves.select().where(
            tables.moves.c.game_id == game_id
        ).order_by(sqlalchemy.desc(tables.moves.c.played))).first()

    if game is None:
        return 'Game not found - something unfixable', 500

    if last_move is None:
        if game['handicap'] > 0:
            next_player_color = 'white'
        else:
            next_player_color = 'black'
    else:
        if last_move['color'] == 'white':
            next_player_color = 'black'
        else:
            next_player_color = 'white'

    if next_player_color != player['color']:
        return 'It\'s not your turn!', 401

    if user_input['x'] > game['size'] or user_input['y'] > game['size']:
        return 'Can\'t play on that spot!', 400

    with tables.get_connection() as connection:
        connection.execute(tables.moves.insert().values(
            game_id=game_id,
            x=user_input['x'],
            y=user_input['y'],
            color=player['color'],
        ))

    return '', 201
