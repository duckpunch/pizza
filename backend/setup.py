from setuptools import find_packages, setup

setup(
    name='pizza',
    version='0.1',
    packages=find_packages(exclude=('tests', 'docs')),
    include_package_data=True,
    install_requires=(
        'Flask',
        'alembic',
        'psycopg2-binary',
        'sqlalchemy',
        'voluptuous',
    ),
)
